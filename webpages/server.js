//npm init -y
//npm install express

const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

// Configuração para servir arquivos estáticos na pasta public
app.use(express.static('public'));

// Roteamento para as diferentes abas
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'home.html'));
});

app.get('/objetivos', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'objetivos.html'));
});

app.get('/desenvolvimento', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'desenvolvimento.html'));
});

app.get('/conclusao', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'conclusao.html'));
});

// Inicia o servidor
app.listen(port, () => {
    console.log(`Servidor rodando em http://localhost:${port}`);
});
